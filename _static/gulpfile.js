var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var uglifycss = require('gulp-uglifycss');

gulp.task('bundlecss', function () {
  return gulp.src(['dev/css/normalize.css', 'dev/css/style.css', 'dev/css/media.css'])
    .pipe(concatCss("bundle.css"))
    .pipe(gulp.dest('dev/css/'));
});


 
gulp.task('uglifycss', function () {
  gulp.src('dev/css/bundle.css')
    .pipe(uglifycss({
      "uglyComments": true
    }))
    .pipe(gulp.dest('dist/css/'));
});

gulp.task('watch', ['bundlecss', 'uglifycss'], function(){
  gulp.watch('dev/css/*.css', ['bundlecss']); 
  gulp.watch('dev/css/bundle.css', ['uglifycss']); 
})